<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SummaryOfOrganization extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'organization_name', 'summary',
    ];
}
