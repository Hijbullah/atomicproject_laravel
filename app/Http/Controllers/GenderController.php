<?php

namespace App\Http\Controllers;

use App\Gender;
use Illuminate\Http\Request;

class GenderController extends Controller
{
    public function store(){
        $objModel = new Gender();
        $objModel->name = $_POST['name'];
        $objModel->gender = $_POST['gender'];
        $status = $objModel->save();

        if ($status)
            echo 'Data has been inserted succesfully';
        else
            echo 'Data has not been inserted';

        return redirect()->route('GenderIndex');
    }// end of store()

    public function index(){
        $objModel = new Gender();
        $allData = $objModel->paginate(5);

        return view('Gender/index', compact('allData'));
    }// end of index()

    public function view($id){
        $objModel = new Gender();
        $oneData = $objModel->find($id);

        return view('Gender/view', compact('oneData'));
    }// end of view()

    public function delete($id){
        $objModel = new Gender();
        $status = $objModel->find($id)->delete();

        return redirect()->route('GenderIndex');
    }// end of delete()

    public function view4Edit($id){
        $objModel = new Gender();
        $oneData = $objModel->find($id);

        return view('Gender/edit', compact('oneData'));
    }// end of view4Edit()

    public function update(){
        $objModel = new Gender();
        $oneData = $objModel->find($_POST['id']);
        $oneData->name = $_POST['name'];
        $oneData->gender = $_POST['gender'];
        $status = $oneData->update();

        return redirect()->route('GenderIndex');

    }// end of update()

    public function search($keyword){
        $objModel = new Gender();
        $searchResult = $objModel
            ->where("name", "LIKE", "%$keyword%")
            ->orwhere("gender", "LIKE", "$keyword%")
            ->paginate(5);

        return view('Gender/search_result', compact('searchResult'));
    }// end of search()
}
