<?php

namespace App\Http\Controllers;

use App\SummaryOfOrganization;
use Illuminate\Http\Request;

class SummaryOfOrganizationController extends Controller
{
    public function store(){
       $objModel = new SummaryOfOrganization();
       $objModel->organization_name = $_POST['organization_name'];
       $objModel->summary = $_POST['summary'];

       $status = $objModel->save();

       return redirect()->route('SummaryOfOrganizationIndex');
    }//end of store()

    public function index(){
        $objModel = new SummaryOfOrganization();
        $allData = $objModel->paginate(5);

        return view('Summary_Of_Organization/index', compact('allData'));
    }//end of index()

    public function view($id){
        $objModel = new SummaryOfOrganization();
        $oneData = $objModel->find($id);

        return view('Summary_Of_Organization/view', compact('oneData'));
    }// end of view()

    public function delete($id){
        $objModel = new SummaryOfOrganization();
        $oneData = $objModel->find($id)->delete();

        return redirect()->route('SummaryOfOrganizationIndex');
    }// end of delete()

    public function view4Edit($id){
        $objModel = new SummaryOfOrganization();
        $oneData = $objModel->find($id);

        return view('Summary_Of_Organization/edit', compact('oneData'));
    }// end of view4Edit()

    public function update(){
        $objModel = new SummaryOfOrganization();
        $oneData = $objModel->find($_POST['id']);
        $oneData->organization_name = $_POST['organization_name'];
        $oneData->summary = $_POST['summary'];

        $status = $oneData->update();

        return redirect()->route('SummaryOfOrganizationIndex');
    }//end of update()

    public function search($keyword){
        $objModel = new SummaryOfOrganization();
        $searchResult = $objModel
            ->where("organization_name", "LIKE", "%$keyword%")
            ->orwhere("summary", "LIKE", "%$keyword%")
            ->paginate(5);

        return view('Summary_Of_Organization/search_result', compact('searchResult'));
    }// end of search()




}
