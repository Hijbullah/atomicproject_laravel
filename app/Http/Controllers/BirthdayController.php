<?php

namespace App\Http\Controllers;

use App\Birthday;
use Illuminate\Http\Request;

class BirthdayController extends Controller
{
    public function store(){
        $objModel = new Birthday();
        $objModel->name = $_POST['name'];
        $objModel->birthday = $_POST['birthday'];

        $status = $objModel->save();
        if ($status)
            echo 'Data has been inserted successfully';
        else
            echo 'Data has not been inserted';

        return redirect()->route('BirthdayIndex');
    }// end of store()

    public function index(){
        $objModel = new Birthday();
        $allData = $objModel->paginate(5);

        return view('Birthday/index', compact('allData'));
    }// end of index()

    public function view($id){
        $objModel = new Birthday();
        $oneData = $objModel->find($id);

        return view('Birthday/view', compact('oneData'));
    }// end of view()

    public function delete($id){
        $objModel = new Birthday();
        $status = $objModel->find($id)->delete();

        return redirect()->route('BirthdayIndex');
    }//end of delete()

    public function view4Edit($id){
        $objModel = new Birthday();
        $oneData = $objModel->find($id);

        return view('Birthday/edit', compact('oneData'));
    }// end of view4Edit()

    public function update(){
        $objModel = new Birthday();
        $oneData = $objModel->find($_POST['id']);
        $oneData->name = $_POST['name'];
        $oneData->birthday = $_POST['birthday'];
        $status = $oneData->update();

        return redirect()->route('BirthdayIndex');
    }//end of update()

    public function search($keyword){
        $objModel = new Birthday();
        $searchResult = $objModel
            ->where("name", "LIKE", "%$keyword%")
            ->orwhere("birthday", "LIKE", "%$keyword%")
            ->paginate(5);

        return view('Birthday/search_result', compact('searchResult'));
    }
}
