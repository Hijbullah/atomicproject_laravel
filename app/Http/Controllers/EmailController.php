<?php

namespace App\Http\Controllers;

use App\Email;
use Illuminate\Http\Request;

class EmailController extends Controller
{

    public function store(){
        $objModel = new Email();
        $objModel->name = $_POST['name'];
        $objModel->email = $_POST['email'];

        $status = $objModel->save();
        if ($status)
            echo 'Data has been inserted Successfully';
        else
            echo 'Data has not beent inserted';

        return redirect()->route('EmailIndex');

    }// end of store()

    public function index(){
        $objModel = new Email();
        $allData = $objModel->paginate(5);

        return view('Email/index', compact('allData'));

    }//end of index()

    public function view($id){
        $objModel = new Email();
        $oneData = $objModel->find($id);

        return view('Email/view', compact('oneData'));
    }// end of view()

    public function delete($id){
        $objModel = new Email();
        $status =   $objModel->find($id)->delete();

        return redirect()->route('EmailIndex');
    }// end of delete()

    public function view4Edit($id){
        $objModel = new Email();
        $oneData = $objModel->find($id);

        return view('Email/edit', compact('oneData'));
    }// end of view4Edit()

    public function update(){
        $objModel = new Email();
        $oneData = $objModel->find($_POST['id']);
        $oneData->name = $_POST['name'];
        $oneData->email = $_POST['email'];

        $status = $oneData->update();

        return redirect()->route("EmailIndex");
    }// end of update()

    public function search($keyword){
        $objModel = new Email();
        $searchResult = $objModel
            ->where("name", "LIKE", "%$keyword%")
            ->orwhere("email", "LIKE", "%$keyword%")
            ->paginate(5);

        return view('Email/search_result', compact('searchResult'));
    }// end of search()

}
