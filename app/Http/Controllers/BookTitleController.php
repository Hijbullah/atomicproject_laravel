<?php

namespace App\Http\Controllers;

use App\BookTitle;
use Illuminate\Http\Request;

class BookTitleController extends Controller
{
    public function store(){

        $objModel = new BookTitle();
        $objModel->book_title = $_POST['book_title'];
        $objModel->author_name = $_POST['author_name'];

        $status = $objModel->save();

        if ($status)
            echo 'Data had been stored successfully';
        else
            echo 'Data has not been stored';

        return redirect()->route('BookTtileCreate');
    }// end of store()

    public function index(){

        $objModel = new BookTitle();

        $allData = $objModel->paginate(30);
        return view('Book_Title/index', compact('allData'));

    }//end of index()

    public function view($id){

        $objModel = new BookTitle();

        $oneData = $objModel->find($id);
        return view('Book_Title/view', compact('oneData'));
    } // end of view()

    public function search($keyword){

        $objModel = new BookTitle();

        $searchResult = $objModel
            ->where("book_title", "LIKE", "%$keyword%")
            ->orwhere("author_name", "LIKE", "%$keyword%")
            ->paginate(30);

        return view('Book_Title/search_result', compact('searchResult'));

    }//end of search()

    public function delete($id){

        $objModel = new BookTitle();
        $status = $objModel->find($id)->delete();

        return redirect()->route('BookTitleIndex');
    } //end of delete()

    public function view4Edit($id){

        $objModel = new BookTitle();

        $oneData = $objModel->find($id);
        return view('Book_Title/edit', compact('oneData'));
    } // end of view4Edit()

     public function update(){

         $objModel = new BookTitle();
         $oneData = $objModel->find($_POST['id']);

         $oneData->book_title = $_POST['book_title'];
         $oneData->author_name = $_POST['author_name'];

         $status = $oneData->update();

         return redirect()->route('BookTitleIndex');

    } // end of update()
}
