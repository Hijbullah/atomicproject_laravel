<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    public function store(){
        $objModel = new City();
        $objModel->name = $_POST['name'];
        $objModel->city = $_POST['city'];
        $status = $objModel->save();

        if ($status)
            echo 'Data has been inserted successfully';
        else
            echo 'Data has not been inserted';

        return redirect()->route('CityIndex');
    }// end of store()

    public function index(){
        $objModel = new City();
        $allData = $objModel->paginate(5);

        return view('City/index', compact('allData'));
    }// end of index()

    public function view($id){
        $objModel = new City();
        $oneData = $objModel->find($id);

        return view('City/view', compact('oneData'));
    }//end of view()

    public function delete($id){
        $objModel = new City();
        $objModel->find($id)->delete();

        return redirect()->route('CityIndex');
    }// end of delete()

    public function view4Edit($id){
        $objModel = new City();
        $oneData = $objModel->find($id);

        return view('City/edit', compact('oneData'));
    }//end of view4Edit()

    public function update(){
        $objModel = new City();
        $oneData = $objModel->find($_POST['id']);

        $oneData->name = $_POST['name'];
        $oneData->city = $_POST['city'];

        $status = $oneData->update();

        return redirect()->route('CityIndex');
    }// end of update()

    public function search($keyword){

        $objModel = new City();

        $searchResult = $objModel
            ->where("name", "LIKE", "%$keyword%")
            ->orwhere("city", "LIKE", "%$keyword%")
            ->paginate(5);

        return view('City/search_result', compact('searchResult'));

    }//end of search()


}
