<?php

namespace App\Http\Controllers;

use App\Hobbies;
use Illuminate\Http\Request;

class HobbiesController extends Controller
{
    public function store(){
        $strHobbies = implode(', ', $_POST['hobbies']);

        $objModel = new Hobbies();
        $objModel->name = $_POST['name'];
        $objModel->hobbies = $strHobbies;

        $status = $objModel->save();
        if ($status)
            echo 'Data has been inserted successfully';
        else
            echo 'Data has not been inserted';

        return redirect()->route('HobbiesIndex');
    }//end of store()

    public function index(){
        $objModel = new Hobbies();
        $allData = $objModel->paginate(5);

        return view('Hobbies/index', compact('allData'));
    }//end of index()

    public function view($id){
        $objModel = new Hobbies();
        $oneData = $objModel->find($id);

        return view('Hobbies/view', compact('oneData'));
    }//end of view()

    public function delete($id){
        $objModel = new Hobbies();
        $status = $objModel->find($id)->delete();

        return redirect()->route('HobbiesIndex');
    }//end of delete()

    public function view4Edit($id){
        $objModel = new Hobbies();
        $oneData = $objModel->find($id);

        $hobbiesArray = explode(', ', $oneData->hobbies);

        return view('Hobbies/edit', compact('oneData','hobbiesArray'));
    }//end of view4Edit()

    public function update(){
        $strHobbies = implode(', ', $_POST['hobbies']);

        $objModel = new Hobbies();
        $oneData = $objModel->find($_POST['id']);
        $oneData->name = $_POST['name'];
        $oneData->hobbies = $strHobbies;

        $status = $oneData->update();
        if ($status)
            echo 'Data has been updated successfully';
        else
            echo 'Data has not been updated';

        return redirect()->route('HobbiesIndex');
    }//end of update()

    public function search($keyword){
        $objModel = new Hobbies();
        $searchResult = $objModel
            ->where("name", "LIKE", "%$keyword%")
            ->orwhere("hobbies", "LIKE", "%$keyword%")
            ->paginate(5);

        return view('Hobbies/search_result', compact('searchResult'));
    }// end of search()

}
