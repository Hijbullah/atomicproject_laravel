@extends('master')


@section('title','Email - Single Email')

@section('header', 'Email | Single Email' )

@section('nav-sub')
    <li><a href="../create"> Create</a></li>
    <li><a href="../index">Active List</a></li>
@endsection


@section('content')
    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <table class="table table-bordered">

                <tr>
                    <th> Name</th>
                    <th> Email</th>
                </tr>
                <tr>
                    <td>{!! $oneData['name'] !!} </td>
                    <td>{!! $oneData['email'] !!} </td>
                </tr>

            </table>
        </div>
    </div>

@endsection
