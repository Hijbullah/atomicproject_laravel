@extends('master')


@section('title','Email - Active List')


@section('header', 'Email | Active List' )

@section ('count-showing')
    Total: {!! $allData->total() !!} Email(s) <br>
    Showing: {!! $allData->count() !!} Email(s) <br>
@endsection

@section('search-bar')
    {!! Form::open(['url'=>'Email/search_result']) !!}


    {!! Form::text('keyword') !!}
    {!! Form::submit('Search') !!}

    {!! Form::close() !!}

@endsection

@section('nav-sub')
    <li><a href="create"> Create</a></li>
    <li><a href="index">Active List</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered table table-striped" >

                <th style="text-align: center">Name</th>
                <th style="text-align: center">Email</th>

                <th style="text-align: center">Action Buttons</th>

                @foreach($allData as $oneData)

                    <tr>

                        <td style="text-align: center">  {!! $oneData['name'] !!} </td>
                        <td style="text-align: center">  {!! $oneData['email'] !!} </td>


                        <td style="text-align: center">
                            <a href="view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                            <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                            <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                        </td>

                    </tr>


                @endforeach


            </table>
            <div class="pagination-bottom text-center">
                {!! $allData->links() !!}
            </div>

        </div>
    </div>



@endsection