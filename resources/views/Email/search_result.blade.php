@extends('master')


@section('title','Email - Search Result')


@section('header', 'Email | Search Result' )

@section ('count-showing')
    Total: {!! $searchResult->total() !!} Email(s) <br>
    Showing: {!! $searchResult->count() !!} Email(s) <br>
@endsection


@section('nav-sub')
    <li><a href="../create"> Create</a></li>
    <li><a href="../index">Active List</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <table class="table table-bordered table table-striped" >

                <th style="text-align: center">Name</th>
                <th style="text-align: center">Email</th>

                <th style="text-align: center">Action Buttons</th>

                @foreach($searchResult as $oneData)

                    <tr>

                        <td style="text-align: center">  {!! $oneData['name'] !!} </td>
                        <td style="text-align: center">  {!! $oneData['email'] !!} </td>

                        <td style="text-align: center">
                            <a href="../view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                            <a href="../edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                            <a href="../delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                        </td>

                    </tr>


                @endforeach



            </table>
            <div class="pagination-bottom text-center">
                {!! $searchResult->links() !!}
            </div>

        </div>
    </div>



@endsection
