@extends('master')

@section('title','Email - Create Form')

@section('header', 'Email | Create Form' )


@section('nav-sub')
    <li><a href="create"> Create</a></li>
    <li><a href="index">Active List</a></li>
@endsection



@section('content')
    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            {!! Form::open(['url'=>'/Email/store', 'method'=>'POST']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('email','Email:') !!}
            {!! Form::email('email','', ['class'=>'form-control', 'required'=>'required']) !!}
            <br>
            <br>
            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}
            <br>

        </div>
    </div>

@endsection
