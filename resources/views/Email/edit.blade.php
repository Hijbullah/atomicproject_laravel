@extends('master')

@section('title','Email - Edit Form')

@section('header', 'Email | Edit Form' )

@section('nav-sub')
    <li><a href="../create"> Create</a></li>
    <li><a href="../index">Active List</a></li>
@endsection


@section('content')
    <div class="row">

        <div class="col-md-8 col-md-offset-2">

            {!! Form::open(['url'=>'/Email/update']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$oneData['name'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('email','Email:') !!}
            {!! Form::email('email',$oneData['email'], ['class'=>'form-control', 'required'=>'required']) !!}
            <br>


            {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

            {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}
            <br>

        </div>
    </div>

@endsection