<!doctype html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset ('css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset ('css/main.css') }}" rel="stylesheet">
    <link href="{{ asset ('css/responsive.css') }}" rel="stylesheet">
    <!--[if lt IE 9]> <script src="{{ asset ('js/html5shiv.js') }}"></script>
    <script src="{{ asset ('js/respond.min.js') }}"></script> <![endif]-->


</head>
<body>

<header id="navigation">
    <div class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="../../../home.php"><h1><img src="../../../images/logo.png" alt="logo"></h1></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="scroll"><a href="../welcome">Home</a></li>
                    <li class="scroll"><a href="../Birthday/index.php">Birthday</a></li>
                    <li class="scroll"><a href="../BookTitle/index.php">Book Title</a></li>
                    <li class="scroll"><a href="../City/index.php">City</a></li>
                    <li class="scroll active"><a href="../Email/index.php">Email</a></li>
                    <li class="scroll"><a href="../Gender/index.php">Gender</a></li>
                    <li class="scroll"><a href="../Hobbies/index.php">Hobbies</a></li>
                    <li class="scroll"><a href="../ProfilePicture/index.php">Profile Picture</a></li>
                    <li class="scroll"><a href="../SummaryOfOrganization/index.php">Summary</a></li>
                </ul>
            </div>
        </div>
    </div><!--/navbar-->
</header> <!--/#navigation-->




<section id="about-us" style="margin-top: 70px; padding: 40px 0">
    <div class="container">
        <div class="text-center">
            <div class="col-sm-8 col-sm-offset-2">
                <h2 class="title-one">@yield('header')</h2>

            </div>
        </div>
        <div id="createAll" class="about-us">
            <div class="row">
                <div class="col-sm-12">
                    <h4>@yield('count-showing')</h4>
                    <!-- required for search, block 4 of 5 start -->

                    <div class="search_bar">
                        @yield('search-bar')
                    </div>

                    <!-- required for search, block 4 of 5 end -->
                    <ul class="nav nav-pills">
                        @yield('nav-sub')
                    </ul>
                    <div class="tab-content other_page">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!--/#about-us-->

<footer id="footer">
    <div class="container">
        <div class="text-center">
            <p>Copyright &copy; 2017 - <a href="https://www.facebook.com/Hijbuu">Hijbullah Amin</a> | All Rights Reserved</p>
        </div>
    </div>
</footer> <!--/#footer-->

<script src="{{ asset('js/jquery-3.2.1.min.css') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/main.js') }}"></script>




</body>
</html>