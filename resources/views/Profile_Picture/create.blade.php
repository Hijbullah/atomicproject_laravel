@extends('master')

@section('title','Profile Picture - Create Form')

@section('header', 'Profile Picture - Create Form' )

@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Profile Picture - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/ProfilePicture/store', 'method'=>'POST']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('email','Email:') !!}
            {!! Form::email('email','', ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
