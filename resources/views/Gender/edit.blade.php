@extends('master')

@section('title','Gender - Edit Form')



@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Gender - Edit Form</h3>
            <hr>

            <br>


            {!! Form::open(['url'=>'/Gender/update', 'method'=>'POST']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$oneData['name'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>
            <div class="row">
                <div class="form-group">
                    <label for="gender" class="col-sm-2 control-label">Gender:</label>
                    <div class="col-sm-10">
                        <div class="radio">
                            <label><input type="radio" id="gender" name="gender" value="Male" <?php if ($oneData['gender'] == "Male") echo "checked"?> >Male</label>
                        </div>
                        <div class="radio">
                            <label><input type="radio" id="gender" name="gender" value="Female"  <?php if ($oneData['gender'] == "Female") echo "checked"?> >Female</label>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <br>
            {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

            {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
