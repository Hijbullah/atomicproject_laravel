@extends('master')

@section('title','Gender - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Gender - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Gender/store', 'method'=>'POST']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('gender','Gender:') !!}
            <br>
            {!! Form::radio('gender','Male', true) !!}Male
            <br>
            {!! Form::radio('gender','Female') !!}Female
            <br>
            <br>
            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
