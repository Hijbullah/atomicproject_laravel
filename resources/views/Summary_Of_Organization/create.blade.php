@extends('master')

@section('title','Summary Of Organization - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Summary Of Organization - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/SummaryOfOrganization/store', 'method'=>'POST']) !!}

            {!! Form::label('organization_name','Organization Name:') !!}
            {!! Form::text('organization_name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('summary','Summary:') !!}
            {!! Form::textarea('summary','', ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
