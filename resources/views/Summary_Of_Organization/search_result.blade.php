@extends('master')


@section('title','Summary Of Organization - Search Result')


@section('content')



    <div class="container">

        <h1>Summary Of Organization - Search Result</h1>


        <div class="navbar">
            <a href="../create"><button type="button" class="btn btn-primary btn-lg">Add New</button></a>
        </div>

        Total: {!! $searchResult->total() !!} Summary(s) <br>

        Showing: {!! $searchResult->count() !!} Summary(s) <br>

        <br>

        <br>
        {!! $searchResult->links() !!}
        <br>

        <table class="table table-bordered table table-striped" >

                    <th>Organization Name</th>
                    <th>Summary</th>
                    <th>Action Buttons</th>

                    @foreach($searchResult as $oneData)

                            <tr>

                                    <td>  {!! $oneData['organization_name'] !!} </td>
                                    <td style="width: 70%">  {!! nl2br($oneData['summary']) !!} </td>
                               
                                    <td>
                                        <a href="../view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                                        <a href="../edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                                        <a href="../delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                                    </td>

                            </tr>


                    @endforeach


            </table>
            {!! $searchResult->links() !!}
    </div>


@endsection
