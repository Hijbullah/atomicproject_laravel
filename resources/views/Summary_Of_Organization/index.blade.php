@extends('master')


@section('title','Summary Of Organization - Active List')


@section('content')

    <h1>Summary Of Organization - Active List</h1>

    <div class="container">

        <div class="navbar">

            <a href="create"><button type="button" class="btn btn-primary btn-lg">Add New</button></a>
        </div>

        {!! Form::open(['url'=>'SummaryOfOrganization/search_result']) !!}


        {!! Form::text('keyword') !!}
        {!! Form::submit('Search',['class'=> 'btn btn-success']) !!}

        {!! Form::close() !!}


        <br>

        Total: {!! $allData->total() !!} Summary(s) <br>

        Showing: {!! $allData->count() !!} Summary(s) <br>

        <br>
        {!! $allData->links() !!}
        <br>


        <table class="table table-bordered table table-striped" >

            <th>Organization Name</th>
            <th>Summary</th>

            <th>Action Buttons</th>

            @foreach($allData as $oneData)

                <tr>

                    <td>  {!! $oneData['organization_name'] !!} </td>
                    <td style="width: 70%">{!! nl2br($oneData['summary']) !!} </td>


                    <td>
                        <a href="view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                        <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                        <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                    </td>

                </tr>


            @endforeach


        </table>

        {!! $allData->links() !!}

    </div>



@endsection