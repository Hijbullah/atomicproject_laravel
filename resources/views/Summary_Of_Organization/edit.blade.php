@extends('master')

@section('title','Summary Of Organization - Edit Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Summary Of Organization - Edit Form</h3>
            <hr>

            {!! Form::open(['url'=>'/SummaryOfOrganization/update', 'method'=>'POST']) !!}

            {!! Form::label('organization_name','Organization Name:') !!}
            {!! Form::text('organization_name',$oneData['organization_name'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('summary','Summary:') !!}
            {!! Form::textarea('summary',$oneData['summary'], ['class'=>'form-control', 'required'=>'required']) !!}
            <br>
            {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}
            {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
