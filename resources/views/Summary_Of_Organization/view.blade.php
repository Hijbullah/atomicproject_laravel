@extends('master')


@section('title','Summary Of Organization - Single Summary')


@section('content')


    <h1> Single Summary Information: </h1>
    <table class="table table-bordered">

       <tr> <td> Organization Name</td> <td> {!! $oneData['organization_name'] !!} </td> </tr>
        <tr> <td> Summary</td> <td>{!! nl2br($oneData['summary']) !!} </td> </tr>

     </table>


@endsection
