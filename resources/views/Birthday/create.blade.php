@extends('master')

@section('title','Birthday - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Birthday - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Birthday/store', 'method'=>'POST']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('birthday','Bithday:') !!}
            {!! Form::date('birthday','', ['class'=>'form-control', 'required'=>'required']) !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
