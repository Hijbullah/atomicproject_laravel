@extends('master')


@section('title','Birthday - Single Book')


@section('content')


    <h1> Single Birthday Information: </h1>
    <table class="table table-bordered">

       <tr>
           <td> Name</td>
           <td> {!! $oneData['name'] !!} </td>
       </tr>
        <tr>
            <td> Birthday</td>
            <td>{!! $oneData['birthday'] !!} </td>
        </tr>

     </table>


@endsection
