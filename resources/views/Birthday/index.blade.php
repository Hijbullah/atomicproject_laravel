@extends('master')


@section('title','Birthday - Active List')


@section('content')

    <h1>Birthday - Active List</h1>

    <div class="container">

        <div class="navbar">

            <a href="create"><button type="button" class="btn btn-primary btn-lg">Add New</button></a>
        </div>

        {!! Form::open(['url'=>'Birthday/search_result']) !!}


        {!! Form::text('keyword') !!}
        {!! Form::submit('Search',['class'=> 'btn btn-success']) !!}

        {!! Form::close() !!}


        <br>

        Total: {!! $allData->total() !!} Birthday(s) <br>

        Showing: {!! $allData->count() !!} Birthday(s) <br>

        <br>
        {!! $allData->links() !!}
        <br>


        <table class="table table-bordered table table-striped" >

            <th>Name</th>
            <th>Birthday</th>

            <th>Action Buttons</th>

            @foreach($allData as $oneData)

                <tr>

                    <td>  {!! $oneData['name'] !!} </td>
                    <td>  {!! $oneData['birthday'] !!} </td>


                    <td>
                        <a href="view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                        <a href="edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                        <a href="delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                    </td>

                </tr>


            @endforeach


        </table>

        {!! $allData->links() !!}

    </div>



@endsection