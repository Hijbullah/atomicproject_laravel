@extends('master')


@section('title','Book Title - Search Result')


@section('content')



    <div class="container">

        <h1>Book Title - Search Result</h1>


        <div class="navbar">
            <a href="../create"><button type="button" class="btn btn-primary btn-lg">Add New</button></a>
        </div>

        Total: {!! $searchResult->total() !!} Book(s) <br>

        Showing: {!! $searchResult->count() !!} Book(s) <br>

        <br>

        <br>
        {!! $searchResult->links() !!}
        <br>

        <table class="table table-bordered table table-striped" >

                    <th>Book Title</th>
                    <th>Author Name</th>
                    <th>Action Buttons</th>

                    @foreach($searchResult as $oneData)

                            <tr>

                                    <td>  {!! $oneData['book_title'] !!} </td>
                                    <td>  {!! $oneData['author_name'] !!} </td>
                               
                                    <td>
                                        <a href="../view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                                        <a href="../edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                                        <a href="../delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                                    </td>

                            </tr>


                    @endforeach


            </table>
            {!! $searchResult->links() !!}
    </div>


@endsection
