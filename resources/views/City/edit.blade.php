@extends('master')

@section('title','City - Edit Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> City - Edit Form</h3>
            <hr>

            {!! Form::open(['url'=>'/City/update', 'method'=>'POST']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$oneData['name'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('city','City:') !!}
            {!!  Form::select('city', [
            'Dhaka' => 'Dhaka',
            'Chittagong' => 'Chittagong',
            'Khulna'=>'Khulna',
            'Rajshahi'=>'Rajshahi',
            'Comilla'=>'Comilla',
            'Sylhet'=>'Sylhet',
            'Barishal'=>'Barishal',
            'Dinajpur'=>'Dinajpur'],
            $oneData['city'],
            ['class'=>'form-control', 'required'=>'required']) !!}

            <br>
            {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
