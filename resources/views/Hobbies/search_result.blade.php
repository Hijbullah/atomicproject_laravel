@extends('master')


@section('title','Hobbies - Search Result')


@section('content')



    <div class="container">

        <h1>Hobbies - Search Result</h1>


        <div class="navbar">
            <a href="../create"><button type="button" class="btn btn-primary btn-lg">Add New</button></a>
        </div>

        Total: {!! $searchResult->total() !!} Hobbies(s) <br>

        Showing: {!! $searchResult->count() !!} Hobbies(s) <br>

        <br>

        <br>
        {!! $searchResult->links() !!}
        <br>

        <table class="table table-bordered table table-striped" >

                    <th>Name</th>
                    <th>Hobbies</th>
                    <th>Action Buttons</th>

                    @foreach($searchResult as $oneData)

                            <tr>

                                    <td>  {!! $oneData['name'] !!} </td>
                                    <td>  {!! $oneData['hobbies'] !!} </td>
                               
                                    <td>
                                        <a href="../view/{!! $oneData['id'] !!}"><button class="btn btn-info">View</button></a>
                                        <a href="../edit/{!! $oneData['id'] !!}"><button class="btn btn-primary">Edit</button></a>
                                        <a href="../delete/{!! $oneData['id'] !!}"><button class="btn btn-danger">Delete</button></a>

                                    </td>

                            </tr>


                    @endforeach


            </table>
            {!! $searchResult->links() !!}
    </div>


@endsection
