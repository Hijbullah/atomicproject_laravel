@extends('master')

@section('title','Hobbies - Create Form')


@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Hobbies - Create Form</h3>
            <hr>

            {!! Form::open(['url'=>'/Hobbies/store', 'method'=>'POST']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name','',['class'=>'form-control', 'required'=>'required']) !!}

            <br>

            {!! Form::label('hobbies','Hobbies:') !!}
            <br>
            {!! Form::checkbox('hobbies[]','Eating','true') !!} Eating &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {!! Form::checkbox('hobbies[]','Riding') !!} Riding &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            {!! Form::checkbox('hobbies[] ','Photography') !!} Photography
            <br>
            <br>

            {!! Form::submit('Submit',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
