@extends('master')

@section('title','Gender - Edit Form')



@section('content')
    <div class="row">

        <div class="col-md-6 col-md-offset-3">

            <h3> Gender - Edit Form</h3>
            <hr>

            <br>


            {!! Form::open(['url'=>'/Hobbies/update', 'method'=>'POST']) !!}

            {!! Form::label('name','Name:') !!}
            {!! Form::text('name',$oneData['name'],['class'=>'form-control', 'required'=>'required']) !!}

            <br>
            <div class="row">
                <div class="form-group">
                    <label for="hobbies" class="col-sm-12 control-label">Hobbies:</label>
                    <br><br>
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <label class="checkbox-inline">
                                <input type="checkbox" id="hobbies" name="hobbies[]"  <?php if(in_array("Eating",$hobbiesArray)) echo "checked" ?>  value="Eating">Eating
                            </label>
                        </div>
                        <div class="col-sm-4">
                            <label class="checkbox-inline">
                                <input type="checkbox" id="hobbies" name="hobbies[]" value="Riding"  <?php if(in_array("Riding",$hobbiesArray)) echo "checked" ?>   >Riding
                            </label>
                        </div>
                        <div class="col-sm-4">
                            <label class="checkbox-inline">
                                <input type="checkbox" id="hobbies" name="hobbies[]" value="Photography"  <?php if(in_array("Photography",$hobbiesArray)) echo "checked" ?> >Photography
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <br>
            {!! Form::text('id',$oneData['id'],['hidden'=>'hidden']) !!}

            {!! Form::submit('Update',['class'=> 'btn btn-success']) !!}

            {!! Form::close() !!}

        </div>
    </div>

@endsection
