@extends('master')


@section('title','Hobbies - Single Email')


@section('content')


    <h1> Single Hobbies Information: </h1>
    <table class="table table-bordered">

       <tr> <td> Name</td> <td> {!! $oneData['name'] !!} </td> </tr>
        <tr> <td> Hobbies</td> <td>{!! $oneData['hobbies'] !!} </td> </tr>

     </table>


@endsection
