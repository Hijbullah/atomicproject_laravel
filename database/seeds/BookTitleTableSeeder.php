<?php

use Illuminate\Database\Seeder;

class BookTitleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=5000; $i++){

            DB::table('book_titles')->insert([
                'book_title' => str_random(10),
                'author_name' => str_random(10)

            ]);
        }

    }
}
