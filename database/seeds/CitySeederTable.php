<?php

use Illuminate\Database\Seeder;

class CitySeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=5000; $i++){
            DB::table('cities')->insert([
                'name' => str_random(10),
                'city' => str_random(10),
            ]);
        }
    }
}
