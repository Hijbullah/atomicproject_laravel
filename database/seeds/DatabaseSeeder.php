<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);

        $this->call(BookTitleTableSeeder::class);
        $this->call(EmailTableSeeder::class);
        $this->call(BirthdaySeederTable::class);
        $this->call(CitySeederTable::class);
    }
}
