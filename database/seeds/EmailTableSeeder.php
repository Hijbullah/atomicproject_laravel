<?php

use Illuminate\Database\Seeder;

class EmailTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=5000; $i++){
            DB::table('emails')->insert([
                'name' => str_random(10),
                'email' => str_random(10).'@gmail.com',
            ]);
        }
    }
}
