<?php

use Illuminate\Database\Seeder;

class BirthdaySeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<=5000; $i++){

            DB::table('birthdays')->insert([
                'name' => str_random(10),
                'birthday' => date('Y-m-d H:i:s'),

            ]);
        }
    }
}
