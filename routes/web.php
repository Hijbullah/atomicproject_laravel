<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', 'HomeController@index')->name('home');


/*Route for BookTitle*/

Route::get('/BookTitle/create', function (){
   return view('Book_Title/create');
})->name('BookTtileCreate');


Route::post('/BookTitle/store', ['uses'=>'BookTitleController@store']);

Route::get('/BookTitle/index', 'BookTitleController@index')->name('BookTitleIndex');

Route::get('/BookTitle/', function (){
    return redirect()->route('BookTitleIndex');
});

Route::get('/BookTitle/view/{id}', 'BookTitleController@view');

Route::get('/BookTitle/search/{keyword}', 'BookTitleController@search');

Route::post('/BookTitle/search_result', function (){
    $path = '/BookTitle/search/'. $_POST['keyword'];
    return redirect($path);
});

Route::get('/BookTitle/delete/{id}', 'BookTitleController@delete');

Route::get('/BookTitle/edit/{id}', 'BookTitleController@view4Edit');

Route::post('/BookTitle/update/', ['uses'=>'BookTitleController@update']);

/*route for BookTitle*/

/*route for Email*/

route::get('/Email/create', function (){
   return view('Email/create');
});

route::post('/Email/store', ['uses'=>'EmailController@store']);

route::get('/Email/index', 'EmailController@index')->name('EmailIndex');

route::get('/Email', function (){
    return redirect()->route('EmailIndex');
});

route::get('/Email/view/{id}', 'EmailController@view');

route::post('/Email/search_result', function (){
   $path = '/Email/search/keyword='. $_POST['keyword'];
   return redirect($path);
});

route::get('/Email/search/keyword={keyword}', 'EmailController@search');

route::get('/Email/delete/{id}', 'EmailController@delete');

route::get('/Email/edit/{id}', 'EmailController@view4Edit');

route::post('/Email/update', ['uses'=>'EmailController@update']);

/*route for Email*/

/*route for Birthday*/

route::get('/Birthday/create', function (){
    return view('Birthday/create');
});

route::post('/Birthday/store', ['uses'=>'BirthdayController@store']);

route::get('/Birthday/index', 'BirthdayController@index')->name('BirthdayIndex');

route::get('/Birthday/view/{id}', 'BirthdayController@view');

route::get('/Birthday/delete/{id}', 'BirthdayController@delete');

route::get('/Birthday/edit/{id}', 'BirthdayController@view4Edit');

route::post('/Birthday/update', ['uses'=>'BirthdayController@update']);

route::post('/Birthday/search_result', function (){
    $path = '/Birthday/search/keyword='. $_POST['keyword'];
    return redirect($path);
});

route::get('/Birthday/search/keyword={keyword}', 'BirthdayController@search');

route::get('/Birthday', function (){
    return redirect()->route('BirthdayIndex');
});

/*route for Birthday*/

/*route for City*/

route::get('/City/create', function (){
    return view('City/create');
});

route::post('/City/store', ['uses'=>'CityController@store']);

route::get('/City/index', 'CityController@index')->name('CityIndex');

route::get('/City/view/{id}', 'CityController@view');

route::get('/City/delete/{id}', 'CityController@delete');

route::get('/City/edit/{id}', 'CityController@view4Edit');

route::post('/City/update', ['uses'=>'CityController@update']);

route::post('/City/search_result', function (){
    $path = '/City/search/keyword='. $_POST['keyword'];
    return redirect($path);
});

route::get('/City/search/keyword={keyword}', 'CityController@search');

route::get('/City', function (){
    return redirect()->route('BirthdayIndex');
});

/*route for City*/

/*route for Gender*/

route::get('/Gender/create', function (){
    return view('Gender/create');
});

route::post('/Gender/store', ['uses'=>'GenderController@store']);

route::get('/Gender/index', 'GenderController@index')->name('GenderIndex');

route::get('/Gender/view/{id}', 'GenderController@view');

route::get('/Gender/delete/{id}', 'GenderController@delete');

route::get('/Gender/edit/{id}', 'GenderController@view4Edit');

route::post('/Gender/update', ['uses'=>'GenderController@update']);

route::post('/Gender/search_result', function (){
    $path = '/Gender/search/keyword='. $_POST['keyword'];
    return redirect($path);
});

route::get('/Gender/search/keyword={keyword}', 'GenderController@search');

route::get('/Gender', function (){
    return redirect()->route('GenderIndex');
});

/*route for Gender*/

/*route for Hobbiea*/

route::get('/Hobbies/create', function (){
    return view('Hobbies/create');
});

route::post('/Hobbies/store', ['uses'=>'HobbiesController@store']);

route::get('/Hobbies/index', 'HobbiesController@index')->name('HobbiesIndex');

route::get('/Hobbies/view/{id}', 'HobbiesController@view');

route::get('/Hobbies/delete/{id}', 'HobbiesController@delete');

route::get('/Hobbies/edit/{id}', 'HobbiesController@view4Edit');

route::post('/Hobbies/update', ['uses'=>'HobbiesController@update']);

route::post('/Hobbies/search_result', function (){
   $path = '/Hobbies/search/keyword='. $_POST['keyword'];
   return redirect($path);
});

route::get('/Hobbies/search/keyword={keyword}', 'HobbiesController@search');

route::get('/Hobbies', function (){
    return redirect()->route('HobbiesIndex');
});

/*route for Hobbiea*/

/*route for SummaryOfOrganization*/

route::get('/SummaryOfOrganization/create', function (){
   return view('Summary_Of_Organization/create');
});

route::post('/SummaryOfOrganization/store', ['uses'=>'SummaryOfOrganizationController@store']);

route::get('/SummaryOfOrganization/index', 'SummaryOfOrganizationController@index')->name('SummaryOfOrganizationIndex');

route::get('/SummaryOfOrganization/view/{id}', 'SummaryOfOrganizationController@view');

route::get('/SummaryOfOrganization/delete/{id}', 'SummaryOfOrganizationController@delete');

route::get('/SummaryOfOrganization/edit/{id}', 'SummaryOfOrganizationController@view4Edit');

route::post('/SummaryOfOrganization/update', ['uses'=>'SummaryOfOrganizationController@update']);

route::post('/SummaryOfOrganization/search_result', function (){
    $path = '/SummaryOfOrganization/search/keyword='. $_POST['keyword'];
    return redirect($path);
});

route::get('/SummaryOfOrganization/search/keyword={keyword}', 'SummaryOfOrganizationController@search');

route::get('/SummaryOfOrganization', function (){
    return redirect()->route('SummaryOfOrganizationIndex');
});

/*route for SummaryOfOrganization*/

/*route for ProfilePicture*/

route::get('/ProfilePicre/create', function (){
    return view('Profile_Picture/create');
});

/*route for ProfilePicture*/